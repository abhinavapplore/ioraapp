

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:iora/screens/delivery.dart';
import 'package:iora/screens/notifications.dart';
import 'package:iora/screens/addDevice.dart';
import 'package:iora/screens/settings.dart';
import 'package:iora/screens/addGroup.dart';
import 'package:iora/screens/myOrder.dart';
import 'package:iora/screens/editProfile.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(fontFamily: 'Poppins'),
    title: 'Navigation Basics',
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> with 
SingleTickerProviderStateMixin{
  int _selectedIndex = 0;  
    final List<Tab> myTabs = <Tab>[
    new Tab(text: 'Home'),
    new Tab(text: 'Group'),
     new Tab(text: 'Purchase'),
    new Tab(text: 'Profile'),
  ];
  TabController tabController;
  @override
void initState(){
super.initState();
tabController = new TabController(length: myTabs.length,vsync: this);
tabController.addListener(_handleTabSelection);
}

  _handleTabSelection() {
                  setState(() {
                    
                    _selectedIndex = tabController.index;
                     print(_selectedIndex);
                  });
              }
@override
void dispose(){
super.dispose();
tabController.dispose();
}
  @override
        Widget build(BuildContext context) {
          

          // Variable declarations
  Color lightGrey =  Color(0xff828282);
   Color mainGrey =  Color(0xff626161);
     Color buttonColor = Color(0xff3e4095);
    int _selectedIndex = 0;  
      
      //Function declarations
  
         Route _notificationsRoute() {
      
          print("HEY");
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => Notifications(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

      Route _groupList(){
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => MyGroupList(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

      Route _createGroupRoute(){
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => AddGroup(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

    Route  _editProfileRoute(){
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => EditProfile(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
    }

     Route _orderRoute(){
        print("HEY");
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => MyOrder(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
     }

      Route _settingsRoute(){
      
          print("HEY");
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => Settings(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

       Route _addDeviceRoute() {
      
          print("HEY");
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => AddDevice(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

      Route _buyProductRoute() {
      
          print("HEY");
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => Delivery(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
      }

    



      //Tabs UI Connections

      
      
          return Scaffold(
          body: TabBarView(
       
           controller: tabController, children: [

             Center(
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/homebg.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),

new Row(
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
  
  new Container(
       margin: EdgeInsets.only(left: 120),
       child: Image.asset('assets/logo.png',
       width:100,
       height:100
       ),
      ),
      GestureDetector(
        child:new Container(
       margin: EdgeInsets.only(left: 100),
       child: Image.asset('assets/bell.png',
       width:30,
       height:30
       
       ),
     
      ),
      onTap: (){
                      Navigator.of(context).push(_notificationsRoute());
      },)
      ],),
      
                Positioned(
                  bottom: 15.0,
                  left: 10.0,
                  right: 10.0,
                  child: Card(
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(
                          
                          padding: EdgeInsets.all(10.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                         
                            child:Padding(
                              padding: EdgeInsets.only(left:15),
                              child:Text(
                             
                            "My devices",
                            style: TextStyle(
                              fontSize: 20,
                              color:Colors.black,
                              fontFamily: 'Poppins-Regular',
                              
                              
                              
                            ),
                          ),))
                        ),
                             new Divider(
                  color: Colors.grey,
                ),
      
                        Padding(
                          padding: const EdgeInsets.only(left:16.0),
                          child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      
                            children:[
                              new Container(
                                child: Image.asset('assets/home-scale.png',
             width:70,
             height:70
             ),
                              ),
                              new Column(
                                 mainAxisSize: MainAxisSize.min,
        children:<Widget>[

    
        new Row(

             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
            Text("Iora 1",
                style: new TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 18.0,
                    fontFamily: "Poppins-Bold"
                ),),
           
               
              Text("Connected",
                style: new TextStyle(
                  color: Colors.white,
                fontSize: 16.0,
                fontFamily: "Poppins-Bold",
                backgroundColor: Color(0xff3e4095)),)
            ],
          ),

       new Row(
       mainAxisAlignment: MainAxisAlignment.start,
        children:[
     Text("Maximum load carrying capacity 15kg.", style:
                new TextStyle(color: Colors.grey,
                 fontFamily: "Poppins-Regular",
                 ),
                 ),
        ]
           
         
      ),
        
        ]
      ),
     
                            
                           
                              
                            ]
                              ),
                        ),
                           new Divider(
                  color: Colors.grey,
                  height: 10,
                ),
                ButtonTheme(
        
        height: 2.0,
        child: FlatButton.icon(
              
                icon: Icon(Icons.add,
                color: Color(0xff3e4095),
                size: 25,), //`Icon` to display
                label: Text('Add device',
                style: TextStyle( fontSize: 18,
                              color:Color(0xff3e4095),
                              fontFamily: 'Poppins-Regular',),), //`Text` to display
                onPressed: () {
                  //Code to execute when Floating Action Button is clicked
                  //...
 Navigator.of(context).push(_addDeviceRoute());

                },),
            ),
      
                      ],
                    ),
                  ),
                ),
              ],
            ),
          
          ),

Center(child: Container(
          width: double.maxFinite,
              height: double.maxFinite,
              decoration: BoxDecoration(
                image: DecorationImage(
        image: AssetImage("assets/main-bg2.png"),
        fit: BoxFit.cover)
              ),
            child: Column(
              children: <Widget>[
            // Image.asset('logo.png',width:300,height:100),
            new Container(
             margin: EdgeInsets.only(top:200),
             child: Image.asset('assets/talk.png',
             width:100,
             height:100
             ),
            ),
           
      
           
              new Container(
              padding: new EdgeInsets.only(top:10),
              child: Text("No Group",
              style: TextStyle(fontSize: 28,
                              color:Colors.black87,
                              fontFamily: 'Poppins-Bold',
                              fontWeight: FontWeight.w800,
                              ),
      
            )
            ),
            new Container(
              padding: new EdgeInsets.only(top:10),
              child: Text("Create your new group.",
              style: TextStyle(fontSize: 22,
                              color:Colors.black87.withOpacity(0.6),
                              fontFamily: 'Poppins-Regular',
                              fontWeight: FontWeight.w800,
                              ),
      
            )
            ),
        
      
            Container(
  margin: const EdgeInsets.only(top: 35.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Create Group',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createGroupRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
      
      
      
           
          ],
            ),
            
          )),
          //TAB3
          Center(child: Container(
         decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)),
        child:Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top:40),
              child:Column(
               
              children: [
          
  Container(
 
    child:Padding(
      padding: EdgeInsets.only(left:20.0,top:20.0),
      child:Text(
                "Select weighing scale type",
                style: TextStyle(color: Colors.black87,
                fontSize: 20,
                fontFamily: "Poppins-Bold",
                
                ),
             
              ),))
 

            ],),),
            Container(
          
            child: SingleChildScrollView(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                
                children: <Widget>[
                 
                
                Container( 
                  
              width: 350,
 margin: EdgeInsets.only(top:100),
                  child:new DropdownButton<String>(
  items: <String>['Iora 1', 'Iora 1', 'Iora 1', 'Iora 1'].map((String value) {
    return new DropdownMenuItem<String>(
      value: value,
      child: new Text(value),
    );
  }).toList(),
  onChanged: (_) {},
))
                  
                 
                  
                ],
              ),
            ),
          ),
            DraggableScrollableSheet(
               initialChildSize: 0.75,
            minChildSize: 0.1,
            maxChildSize: 0.77,
              builder: (BuildContext context, ScrollController scrollController){
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(30), topLeft: Radius.circular(30.0)),
                    color: Colors.white,

                  ),
                  child: Container(
                    padding: EdgeInsets.only(top: 30.0,left: 20),
                   
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
           
                      children: [

                     Text(
                "Iora 1 weighing scales",
                style: TextStyle(color: buttonColor,
                fontSize: 20,
                fontFamily: "Poppins-Bold",
                
                ),
             
              ),
                 Container(
                   margin: EdgeInsets.only(top:25.0),
                   child:Text(
                "- Set of 4 weighing scales",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
                 Container(
                   margin: EdgeInsets.only(top:5.0),
                   child:Text(
                '- Size 16" X 16" each scale of 8" X 8"',
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),)
             
              ),   Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- 4 weighing scales with color coding red, green, blue, yellow.",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),  Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- Maximum load carrying capacity 15kg",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
              Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- Single PCB will operate with this scale",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "₹ 5000",
                style: TextStyle(color: buttonColor,
                fontSize: 16,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
              Container(
  margin: const EdgeInsets.all(10.0),
     width: 150.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Buy now',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_buyProductRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
              ],)

                  ],),)
                );
              },
            )
        ],),),),
          //TAB 4
          Center(child: Container(
          width: double.maxFinite,
              height: double.maxFinite,
              decoration: BoxDecoration(
                image: DecorationImage(
        image: AssetImage("assets/main-bg2.png"),
        fit: BoxFit.cover)
              ),
            child: Column(
              children: <Widget>[
            
      
           
              new Container(
              padding: new EdgeInsets.only(top:40),
              child: Text("Ami A. Nehete",
              style: TextStyle(fontSize: 22,
                              color:Colors.black,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                              ),
      
            )
            ),
      new Container(
             padding: new EdgeInsets.only(top:5),
              child: Text("+91-0123456789",
              style: TextStyle(
                fontSize: 16,
                              color:Color(0xff828282).withOpacity((0.8)),
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                              ),
      
            )
            ),
            new Container(
             padding: new EdgeInsets.only(top:5),
              child: Text("amitest@gmail.com",
              style: TextStyle(fontSize: 16,
                              color:Color(0xff828282).withOpacity(0.8),
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                              ),
      
            )
            ),
      
             Container(
               alignment: Alignment.center,
               margin: EdgeInsets.only(top:10),
               child: Column(
             children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        
        children: [
          Container(
             alignment: Alignment.center,
             child: Row(children: [
      Container(
          padding: EdgeInsets.all(10),
         width:20,
         height: 20,
         decoration: BoxDecoration(
           image: DecorationImage(
             image:AssetImage("assets/tap1.png",
             
             ),
             
             
           ) 
         ),
        
        ),
       Container( 
          padding: EdgeInsets.all(10),
         child:Text("Subscribe Plan",
        textAlign: TextAlign.center,
        style: TextStyle(
      
          color: Color(0xff3e4095),
        fontSize: 16,
        fontFamily: 'Poppins'
        ),
        )
       
        ),
      
        Container(
          height: 35,
          padding: EdgeInsets.only(top: 5,right: 10),
          child: VerticalDivider(
            color: Color(0xff3e4095),
            thickness: 2,
          )),
      
      
      
      
       
      
         InkWell(
   onTap: (){
                       Navigator.of(context).push(_editProfileRoute());
                     },
           child:Container(
            padding: EdgeInsets.all(10),
         width:20,
         height: 20,
         decoration: BoxDecoration(
           image: DecorationImage(
             image:AssetImage("assets/edit1.png",
             
             ),
             
             
           ) 
         ),
        
        ),),
        InkWell(
   onTap: (){
                       Navigator.of(context).push(_editProfileRoute());
                     },
          child:Container(
           padding: EdgeInsets.all(10), 
          child:Text("Edit Profile",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Color(0xff3e4095),
        fontSize: 16,
        fontFamily: 'Poppins'),)),)

         
             ],
             ),

             ),

            
        
       
      
       
      ],
      ),
      
      Container(
        padding: EdgeInsets.all(10),
        child:Card(
               child: Column(
                 mainAxisSize: MainAxisSize.max,
                 children:[
                   InkWell(child:Padding(
                     padding: EdgeInsets.all((20)),
                     child:Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children:[
                    Text("My Order",
                    style: TextStyle(
          color: lightGrey,
        fontSize: 18,
        fontFamily: 'Poppins'),),
                     Icon(Icons.keyboard_arrow_right_outlined,
                     size: 40,
                     color: lightGrey,)
                     
                     ]
                   
                     )),
                     onTap: (){
                       Navigator.of(context).push(_orderRoute());
                     },
                     ),
                      InkWell(child:Padding(
                     padding: EdgeInsets.all((20)),
                     child:Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children:[
                    Text("Group",
                    style: TextStyle(
          color: lightGrey,
        fontSize: 18,
        fontFamily: 'Poppins'),),
                     Icon(Icons.keyboard_arrow_right_outlined,
                     size: 40,
                     color: lightGrey,)
                     ]
                   
                     )),
                     onTap: (){
                       Navigator.of(context).push(_groupList());
                     },
                     ),
                     InkWell(child:Padding(
                     padding: EdgeInsets.all((20)),
                     child:Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children:[
                    Text("Settings",
                    style: TextStyle(
          color: lightGrey,
        fontSize: 18,
        fontFamily: 'Poppins'),),
                    Icon(Icons.keyboard_arrow_right_outlined,
                     size: 40,
                     color: lightGrey,)
                     ]
                   
                     )),
                     onTap: (){
                       Navigator.of(context).push(_settingsRoute());
                     },
                     ),
                     Padding(
                     padding: EdgeInsets.all((20)),
                     child:Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children:[
                    Text("Guide",
                    style: TextStyle(
          color: lightGrey,
        fontSize: 18,
        fontFamily: 'Poppins'),),
                     Icon(Icons.keyboard_arrow_right_outlined,
                     size: 40,
                     color: lightGrey,)
                     ]
                   
                     )),
                     Padding(
                     padding: EdgeInsets.all((20)),
                     child:Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children:[
                    Text("Privacy Policy",
                    style: TextStyle(
          color: lightGrey,
        fontSize: 18,
        fontFamily: 'Poppins'),),
                     Icon(Icons.keyboard_arrow_right_outlined,
                     size: 40,
                     color: lightGrey,)
                     ]
                   
                     )),
                 ]
               ),))
      
      
      
             ],
            ),
            
      
             )  
      
            
      
      
      
           
          ],
            ),
            
          ),
          ),
          

             
           ],
          ),
         bottomNavigationBar:new Material(
color: Colors.white,
child: new TabBar(
controller: tabController,


            labelColor: buttonColor,
            unselectedLabelColor: Colors.grey,
            tabs: [
              Tab(
                  text: 'Home',
                  icon: Icon(Icons.home_outlined,
                      color: tabController.index == 0
                          ? buttonColor
                          : Colors.black)),
              Tab(
                  text: 'Group',
                  icon: Icon(Icons.group_work_outlined,
                      color: tabController.index == 1
                          ? buttonColor
                          : Colors.grey)),
              Tab(
                  text: 'Purchase',
                  icon: Icon(Icons.speed_outlined,
                      color: tabController.index  == 2
                          ? buttonColor
                          : Colors.grey)),
                          Tab(
                  text: 'Profile',
                  icon: Icon(Icons.person,
                      color: tabController.index  == 3
                          ? buttonColor
                          : Colors.grey)),
            ],
),
)
);
          //   bottomNavigationBar: BottomNavigationBar(
          //     currentIndex: _selectedIndex,
          //     type: BottomNavigationBarType.fixed,
          //     iconSize: 30,
          //     items: [
          //       BottomNavigationBarItem(icon: 
          //       Icon(Icons.home_outlined),
          //       title: Text('Home'),
          //       backgroundColor: Color(0xff3e4095),
          //       ),
          //        BottomNavigationBarItem(icon: 
          //       Icon(Icons.group_work ),
          //       title: Text('Group'),
          //       backgroundColor: Color(0xff3e4095),
          //       ),
          //        BottomNavigationBarItem(icon: 
          //       Icon(Icons.speed_outlined),
          //       title: Text('Purchase'),
          //       backgroundColor: Color(0xff3e4095),
          //       ),
          //        BottomNavigationBarItem(icon: 
          //       Icon(Icons.person),
          //       title: Text('Profile'),
          //       backgroundColor: Color(0xff3e4095),
          //       ),
          //     ],
          //     onTap: (index){
          //       setState(() {
          //   _selectedIndex = index;
          //   print(_selectedIndex);
          // });
              
          //     },
          //   ),
          // ),
        

          // );
 
      
      
      }
      
} 

class MyGroupList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return MyGroupListState();

    
  }
}
class MyGroupListState extends State <MyGroupList>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
//  Route _createSuccessRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => RegisterSuccess(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
   extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "My Groups",
                style: TextStyle(color: Colors.black,
                fontSize: 26,
                fontFamily: "Poppins",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: Container(
    margin: EdgeInsets.only(top:100),
    child:(Column(
mainAxisSize: MainAxisSize.min,
crossAxisAlignment: CrossAxisAlignment.start,
    

      children :[
       
Column(
  mainAxisSize: MainAxisSize.max,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
Padding(
  padding: EdgeInsets.all(10),
  child:Card(
  margin: EdgeInsets.all(10),
  child: Column(children: [

    Padding(
      padding: EdgeInsets.all(10),
      child:new Row(
       mainAxisAlignment :MainAxisAlignment.spaceBetween,
        children:[
Text("Group subject",
        style: TextStyle(fontSize: 18,
                        color:buttonColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,),

      ),
     Icon(Icons.arrow_drop_down_outlined)
        ] 
      ),),

  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.only(left:10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:2,top:2),
        child: Text("2 customers",
        style: TextStyle(fontSize: 16,
                          color:Colors.black.withOpacity(0.7),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
    
  ],),
  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.only(left:10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:2,top:2),
        child: Text("Weighing scale name",
        style: TextStyle(fontSize: 16,
                        color:Colors.black.withOpacity(0.7),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
    
  ],)
],),)),

],),
Column(
  mainAxisSize: MainAxisSize.max,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
Padding(
  padding: EdgeInsets.all(10),
  child:Card(
  margin: EdgeInsets.all(10),
  child: Column(children: [

    Padding(
      padding: EdgeInsets.all(10),
      child:new Row(
       mainAxisAlignment :MainAxisAlignment.spaceBetween,
        children:[
Text("Group subject",
        style: TextStyle(fontSize: 18,
                        color:buttonColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,),

      ),
     Icon(Icons.arrow_drop_down_outlined)
        ] 
      ),),

  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.only(left:10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:2,top:2),
        child: Text("2 customers",
        style: TextStyle(fontSize: 16,
                          color:Colors.black.withOpacity(0.7),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
    
  ],),
  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.only(left:10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:2,top:2),
        child: Text("Weighing scale name",
        style: TextStyle(fontSize: 16,
                        color:Colors.black.withOpacity(0.7),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
    
  ],)
],),)),

],)
          
      ]
    )
    )
      
    )
),
    );
  }
}

