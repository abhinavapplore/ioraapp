
import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';



class Delivery extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DeliveryState();

    
  }
}
class DeliveryState extends State <Delivery>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
  var firstName = TextEditingController();
  var middleName = TextEditingController();
  var lastName = TextEditingController();
  var deliveryAddress = TextEditingController();
  var mobileNumber = TextEditingController();
  var emailAddress = TextEditingController();


  

 Route _orderDetailRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => DeliveryConfirm(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

// Route _createHomeRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }



// Route _createForgotRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => ForgotPassword(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

 @override
 
 
   @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    firstName.dispose();
     middleName.dispose();
      lastName.dispose();
       deliveryAddress.dispose();
        mobileNumber.dispose();
        
            emailAddress.dispose();
           
    super.dispose();
  }

 

   String validateEmail(String value) {
      if (value.isEmpty) {
        return 'Email is required.';
      }

      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email';
      else
        return null;
    }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
       extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Delivery Address",
                style: TextStyle(color: Colors.black,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      body: new Container(
         decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/main-bg2.png'),
                fit: BoxFit.cover,
              ),
            ),
      padding: EdgeInsets.only(left: 40,right: 40,top:100),

      child:Form(  
        
      key: _formKey,  
      child: Column(  
        
        crossAxisAlignment: CrossAxisAlignment.start,  
        children: <Widget>[  
         
        
       
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'First name',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: firstName,

             validator: (value) {
              if (value.isEmpty) {
                return 'First name is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Middle name',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: middleName,

             validator: (value) {
              if (value.isEmpty) {
                return 'Middle name is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
            TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Surname',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: lastName,

             validator: (value) {
              if (value.isEmpty) {
                return 'Surname is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Address',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: deliveryAddress,

             validator: (value) {
              if (value.isEmpty) {
                return 'Address is required.';
              }else{
                print(value);
              }
              return null;
            },
           ), 
TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Mobile number',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            keyboardType: TextInputType.number,
            controller: mobileNumber,

             validator: (value) {
              if (value.isEmpty) {
                return 'Mobile number is required.';
              }else if(value.length<10){
                return 'Please enter 10 digit mobile number.';
              }else{
                print(value);
              }
              return null;
            },
           ), 
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Email address',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: emailAddress,

            

             validator: validateEmail
           ), 
            
           
           Container(
  margin: const EdgeInsets.only(top: 150.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Submit Address',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
    if (_formKey.currentState.validate()) {
                  // Process data.
                    // Process data.
                var userObj ={
                  'firstName':firstName.text,
                  'middleName':middleName.text,
                  'lastName':lastName.text,
                  'deliveryAddress':deliveryAddress.text,
                  'mobileNumber':mobileNumber.text,
                  'emailAddress':emailAddress.text,
                 
                };
                print(userObj);
              
                Navigator.of(context).push(_orderDetailRoute());
                }
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
           
        ],  
      ),  
    )
    ),
    );
  }
}


class DeliveryConfirm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DeliveryConfirmState();

    
  }
}
class DeliveryConfirmState extends State <DeliveryConfirm>{

Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

  

//  Route _orderDetailRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => Register(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

Route _successOrderPage() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AddOrderSuccess(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
       extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Delivery Address",
                style: TextStyle(color: Colors.black,
                fontSize: 24,
                fontFamily: "Poppins",
                ),
              
              ),
             
            ),
      body: Container(
        padding: EdgeInsets.only(top:100,left: 20,right: 20),
         decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/main-bg2.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(children: [
              Row(
                children: [
                  Text(
                "Order Details : ",
                style: TextStyle(color: Colors.black,
                fontSize: 18,
                fontFamily: "Poppins",
                ),
              
              ),
              ],),
               Card(
                 margin: EdgeInsets.only(top:10),
                 
                 child: Padding(
                   padding: EdgeInsets.all(20),
                   child:Column(
              
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                   
Text(
                "Iora 1 weighing scales",
                style: TextStyle(color: buttonColor,
                fontSize: 16,
                fontFamily: "Poppins",
                
                ),
             
              ),
                 Container(
                   margin: EdgeInsets.only(top:25.0),
                   child:Text(
                "- Set of 4 weighing scales",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
                 Container(
                   margin: EdgeInsets.only(top:5.0),
                   child:Text(
                '- Size 16" X 16" each scale of 8" X 8"',
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),)
             
              ),   Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- 4 weighing scales with color coding red, green, blue, yellow.",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),  Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- Maximum load carrying capacity 15kg",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
              Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "- Single PCB will operate with this scale",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
               ],)),),
                Row(
                children: [
                  Text(
                "Delivery Address : ",
                style: TextStyle(color: Colors.black,
                fontSize: 18,
                fontFamily: "Poppins",
                ),
              
              ),
              ],),
               Card(
                 margin: EdgeInsets.only(top:10),
                 
                 child: Padding(
                   padding: EdgeInsets.all(20),
                   child:Column(
              
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                   
Text(
                "Ami A Nehete",
                style: TextStyle(color: buttonColor,
                fontSize: 16,
                fontFamily: "Poppins",
                
                ),
             
              ),
                 Container(
                   margin: EdgeInsets.only(top:25.0),
                   child:Text(
                "128 ,Nitin Ind Compound, Vaishali Nagar, Warje, Pune.",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),
                 Container(
                   margin: EdgeInsets.only(top:5.0),
                   child:Text(
                'Mob : -91 -123456789',
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),)
             
              ),   Container(
                margin: EdgeInsets.only(top:5.0),
                child:Text(
                "Email : amitest@gmail.com",
                style: TextStyle(color: Colors.black87.withOpacity(0.7),
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                
                ),
             
              ),),  
              
               ],)),),
                Row(
                children: [
                  Text(
                "Payment Details : ",
                style: TextStyle(color: Colors.black,
                fontSize: 18,
                fontFamily: "Poppins",
                ),
              
              ),
              ],),
               Card(
                   margin: EdgeInsets.only(top:10),
                 child:Padding(
                      padding: EdgeInsets.all(20),
                   child:Column(children: [
Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 
                 children:[
Text(
                "Item Price",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),
              Text(
                "₹ 5000",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),


                 ]),
                 Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 
                 children:[
Text(
                "Delivery Charges",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),
              Text(
                "₹ 50",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),


                 ]),
                 Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 
                 children:[
Text(
                "GST Amount",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),
              Text(
                "₹ 500",
                style: TextStyle(color: inputColor,
                fontSize: 14,
                fontFamily: "Poppins",
                
                ),
             
              ),


                 ]),
                 Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 
                 children:[
Text(
                "Order Total",
                style: TextStyle(color: Colors.black,
                fontSize: 16,
                fontFamily: "Poppins",
                
                ),
             
              ),
              Text(
                "₹ 5550",
                style: TextStyle(color: Colors.black,
                fontSize: 16,
                fontFamily: "Poppins",
                
                ),
             
              ),


                 ]),

               ],))
               ),
 Container(
  margin: const EdgeInsets.only(top: 15.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Pay Now',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_successOrderPage());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
            ],),
      )
    );
  }
}


class AddOrderSuccess extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddOrderSuccessState();

    
  }
}



class AddOrderSuccessState extends State <AddOrderSuccess>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
 bool _obscureText = true;

  

 





Route _createHomeNavRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
       extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
             
             
            ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      new Container(


        margin: new EdgeInsets.only(top:100),
       child: Image.asset('assets/check.png',
       width:100,
       height:100
       ),
      ),

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Success !",
        style: TextStyle(fontSize: 30,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),

      
        new Container(
alignment: Alignment.center,
        padding: new EdgeInsets.only(left:100,right: 100,top: 20),
        child: Text("Your order has been confirmed thank you for your purchase.",
        style: TextStyle(fontSize: 15,
                        color:Colors.black.withOpacity(0.6),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),
textAlign: TextAlign.center,
      )
      ),

       Align(
         alignment: Alignment.bottomCenter,
         child:Container(
  
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Back To Home',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createHomeNavRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       )),
     




     
    ],
  ),
),
    );
  }
}


