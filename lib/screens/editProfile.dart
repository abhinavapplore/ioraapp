import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';






class EditProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return EditProfileState();

    
  }
}

class EditProfileState extends State <EditProfile>{
  var myName = TextEditingController();
  var myAddress = TextEditingController();
  var myLandmark = TextEditingController();
  var myCity = TextEditingController();
  var myDistrict = TextEditingController();
  var myState = TextEditingController();
   var myPincode = TextEditingController();
   var myMobile = TextEditingController();
   var myEmail = TextEditingController();
   var myPassword = TextEditingController();
  final myConfirmedPass = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Color buttonColor = Color(0xff3e4095);
  Color inputColor = Color(0xff626161);
 
  ScrollController _controller;

  Route _createTab4Route() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}


  @override
 
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }
   @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myName.dispose();
     myAddress.dispose();
      myCity.dispose();
       myDistrict.dispose();
        myDistrict.dispose();
         myState.dispose();
          myPincode.dispose();
           myMobile.dispose();
            myEmail.dispose();
             myPassword.dispose();
              myConfirmedPass.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the bottom";
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the top";
      });
    }
  }

   String validateEmail(String value) {
      if (value.isEmpty) {
        return 'Email is required.';
      }

      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email';
      else
        return null;
    }

  Widget build(BuildContext context) {
   
    return Scaffold(
      
      resizeToAvoidBottomPadding: true,
       extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Edit Profile",
                style: TextStyle(color: Colors.black,
                fontSize: 26,
                fontFamily: "Poppins",
                ),
              
              ),
             
            ),
     body:    
     
     
     Container(
       
        decoration: BoxDecoration(
          image: DecorationImage(
            
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
            width: double.maxFinite,
        height: double.maxFinite, 
      
  child: new ListView(
     controller: _controller,
  
    children: <Widget>[

       
    
    new Container(
      padding: EdgeInsets.only(left: 20,right: 20),
      child:Form(  
        
      key: _formKey,  
      child: Column(  
        
        crossAxisAlignment: CrossAxisAlignment.start,  
        children: <Widget>[  
         TextFormField(  
          enabled:false,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Ami',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myName,

             validator: (value) {
              if (value.isEmpty) {
                return 'Name is required.';
              }else{
                print(value);
              }
              return null;
            },
             onSaved: (value) {
        setState(() {
       
        });
      },
           ),   
        TextFormField(  
             enabled:false,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'A.',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myAddress,

             validator: (value) {
              if (value.isEmpty) {
                return 'Address is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),   
         TextFormField(  
           enabled:false,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Nehete',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myLandmark,

           ),  
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: '128 , ARO building, Warje, Pune',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myCity,

             validator: (value) {
              if (value.isEmpty) {
                return 'City is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
           TextFormField(  
           enabled:false,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: '+91 - 123456789',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myDistrict,

             validator: (value) {
              if (value.isEmpty) {
                return 'District is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
          
TextFormField(  
           enabled: false,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'amitest@gmail.com',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            keyboardType: TextInputType.text,
            controller: myMobile,

             validator: (value) {
              if (value.isEmpty) {
                return 'Mobile number is required.';
              }else if(value.length<10){
                return 'Please enter 10 digit mobile number.';
              }else{
                print(value);
              }
              return null;
            },
           ), 

              Container(
               
                alignment: Alignment.centerLeft,
                child:Card(
                 
        elevation: 0,
      color: Colors.white,
      child: Text("CHANGE PASSWORD :",
      style: TextStyle(color: Colors.black,fontSize: 20),),
              ),),

            TextFormField(
            obscureText: true,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
              suffixIcon: const Icon(Icons.visibility_outlined,
              color: Colors.grey),
              hintText: 'Current Password',
             
            ),
            controller: myPassword,

            validator: (value) {
              if (value.isEmpty) {
                return 'Current Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }else{
                print(value);
              }
              return null;
            },
            onSaved: (value) {
        setState(() {
      
        });
      },
          ),
          
            TextFormField(
            obscureText: true,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
              suffixIcon: const Icon(Icons.visibility_outlined,
              color: Colors.grey),
              hintText: 'Password',
             
            ),
            controller: myPassword,

            validator: (value) {
              if (value.isEmpty) {
                return 'Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }else{
                print(value);
              }
              return null;
            },
            onSaved: (value) {
        setState(() {
      
        });
      },
          ),
           TextFormField(
            obscureText: true,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
              suffixIcon: const Icon(Icons.visibility_outlined,
              color: Colors.grey),
              hintText: 'Confirm password',
             
            ),
            controller: myConfirmedPass,

            validator: (value) {
              if (value.isEmpty) {
                return 'Confirm Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }
              else if(value!=myPassword.text){
                return 'Password does not match!';
              }
              return null;
            },
          ),
           Center(child:Container(
        
  margin: const EdgeInsets.only(top: 25.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Save Changes',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createTab4Route());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),)
           
        ],  
      ),  
    )
    ),

   
      

  
   
     
   

    
      
    ],
  ),
),
      
   
    );
  }
}