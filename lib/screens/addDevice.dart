import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';


void main() {
  runApp(MaterialApp(
     debugShowCheckedModeBanner: false,
    theme: ThemeData(fontFamily: 'Poppins'),
    title: 'Navigation Basics',
    home: AddDevice(),
  ));
}

//My LOGIN PAGE
class AddDevice extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddDeviceState();

    
  }
}
class AddDeviceState extends State <AddDevice>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

Route _createDeviceSuccessRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AddDeviceSuccess(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}
  

 Route _createAddDeviceSuccess() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AddDeviceSuccess(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

Route _createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Scan device QR",
                style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      InkWell( 
        child:new Container(
        padding: new EdgeInsets.only(top:250),
       child: Image.asset('assets/qr.png',
       width:200,
       height:200
       ),
      ),
      onTap: (){
         Navigator.of(context).push((_createDeviceSuccessRoute()));
      },
      )

     
    ],
  ),
),
    );
  }
}


class AddDeviceSuccess extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddDeviceSuccessState();

    
  }
}
class AddDeviceSuccessState extends State <AddDeviceSuccess>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
 bool _obscureText = true;

  

 Route _createAddDeviceSuccess() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AddDeviceSuccess(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

Route _createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}



Route _createHomeNavRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
       extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
             
             
            ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      new Container(


        margin: new EdgeInsets.only(top:100),
       child: Image.asset('assets/check.png',
       width:100,
       height:100
       ),
      ),

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Success !",
        style: TextStyle(fontSize: 30,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),
        new Container(

        padding: new EdgeInsets.only(top:10),
        child: Text("Your device connected successfully !",
        style: TextStyle(fontSize: 15,
                        color:Colors.black.withOpacity(0.6),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),

      )
      ),
      new Container(
      
        margin: new EdgeInsets.only(top:10),
        padding: new EdgeInsets.all(20),
        child: new Column(

          children:[
  new Divider(
                  color: Colors.grey,
                  
                ),

                Container(
                 
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(top:20),
                  child: Text("How it works ?",
                  style: TextStyle(color:Colors.black.withOpacity(0.9),
                        fontFamily: 'Poppins-Regular',
                        fontWeight: FontWeight.w600,
                        fontSize: 24)
                        ,
                        ),
                ),
                Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top:20),
                  child: Card(
                    
                    child: Image.asset('assets/tick.png',
       width:100,
       height:100
       ),)
                ),

                Container(
  margin: const EdgeInsets.only(top: 20.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    
    width: double.infinity,
    child: Padding(
      padding: EdgeInsets.only(left:10),
      child:Text(
    
      'Back To Home',
      textAlign: TextAlign.left,
    ),)
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createHomeRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
          ]
        )
      
      ),




     
    ],
  ),
),
    );
  }
}
