
import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';
import 'package:iora/screens/introduction.dart';


class Settings extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return SettingsState();

    
  }
}
class SettingsState extends State <Settings>{
 bool switchControl = false;
  var textHolder = 'Switch is OFF';
 
  void toggleSwitch(bool value) {
 
      if(switchControl == false)
      {
        setState(() {
          switchControl = true;
          textHolder = 'Switch is ON';
        });
        print('Switch is ON');
        // Put your code here which you want to execute on Switch ON event.
 
      }
      else
      {
        setState(() {
          switchControl = false;
           textHolder = 'Switch is OFF';
        });
        print('Switch is OFF');
        // Put your code here which you want to execute on Switch OFF event.
      }
  }

  Route _addLogoutRoute(){
     return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => FirstRoute(),
         transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return SlideTransition(
                          transformHitTests: false,
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: new SlideTransition(
                            position: new Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(-1.0, 0.0),
                            ).animate(secondaryAnimation),
                            child: child,
                          ),
                        );
      },
        );
  }
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
//  Route _createSuccessRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => RegisterSuccess(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
   extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Setting",
                style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: Container(
    margin: EdgeInsets.only(top:100),
    alignment: Alignment.center,
    child: Column(
      
      children: [
        Container(
          color: Colors.white,
         margin: EdgeInsets.all(20),
          child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
          Container(
            padding: EdgeInsets.all(20),
            height: 80,
            alignment: Alignment.centerLeft,
            child: Text("Notifications",
             style: TextStyle(
                              fontSize: 20,
                              color:Colors.black,
                              fontFamily: 'Poppins-Regular',
                              
                              
                              
                            ),),
          ),

          Container(
            padding: EdgeInsets.all(20),
             alignment: Alignment.centerLeft,
               height: 80,
            child:Transform.scale( 
              scale: 1.5,
              child: Switch(
              onChanged: toggleSwitch,
              value: switchControl,
              activeColor: Colors.green,
              activeTrackColor: Colors.grey,
              inactiveThumbColor: Colors.white,
              inactiveTrackColor: Colors.grey,
            )
          ), )
          
        ],)),

        Container(
          color: Colors.white,
         margin: EdgeInsets.all(20),
          child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
          InkWell(
            onTap: (){
              Navigator.of(context).push(_addLogoutRoute());
            },
            child:Container(
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            child: Text("Logout",
             style: TextStyle(
                              fontSize: 20,
                              color:Colors.black,
                              fontFamily: 'Poppins-Regular',
                              
                              
                              
                            ),),
          ),)

         
          
        ],))
      ],),
      )
      
    
),
    );
  }
}