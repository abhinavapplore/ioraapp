import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';

import 'package:carousel_slider/carousel_slider.dart';

import 'package:http/http.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;


void main() {
  runApp(MaterialApp(
     debugShowCheckedModeBanner: false,
    theme: ThemeData(fontFamily: 'Poppins'),
    title: 'Navigation Basics',
    home: FirstRoute(),
  ));
}


//USER WELCOME PAGE
class FirstRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FirstRouteState();

    
  }
}
 class FirstRouteState extends State <FirstRoute>{


   Color buttonColor = Color(0xff3e4095);
Color headColor = Color(0xff3E4095);
Color subColor = Color(0xff626161);
int _current = 0;

var sliderDetail = [
  {'heading':"Monitoring Weighing Scale",'subHeading':"Lorem Ipsum is simply the dummy text of printing and typesetting industry."},
  {'heading':"Monitoring Weighing Scale",'subHeading':"Lorem Ipsum is simply the dummy text of printing and typesetting industry."},
  {'heading':"Monitoring Weighing Scale",'subHeading':"Lorem Ipsum is simply the dummy text of printing and typesetting industry."}
];

Route _createLoginRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Login(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

Route _createRegisterRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Register(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}


    @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      // appBar: AppBar(
      //   title: Text('Landing'),
      // ),

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
       Image.asset('assets/logo.png',width:200,height:200),

      new Container(
         margin: const EdgeInsets.only(top: 40.0),
          width: double.infinity,
        child:  CarouselSlider(
 
  options: CarouselOptions(
    
  height: 200.0,
  
  initialPage: 0,
  autoPlay: true,
  autoPlayInterval: Duration(seconds: 3),
  autoPlayAnimationDuration: Duration(milliseconds: 800),
  autoPlayCurve: Curves.easeInOut,
  
  onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }
  ),
  items: sliderDetail.map((choice){
    
    return Builder(
      builder: (BuildContext context) {
        return Container(
          width: double.infinity,
         
          child: Column(
           children:[
            Text(
        choice['heading'],
        style: TextStyle(fontSize: 20,
                        color:headColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,
                        
                        ),
        textAlign: TextAlign.center,
       
      ), Container(
        margin: const EdgeInsets.only(top: 20.0),
        child: Text(
        
        choice['subHeading'],
        style: TextStyle(fontSize: 20,
                        color:subColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,
                      
                        ),
        textAlign: TextAlign.center,
       
      ),
      
      )
      
           ] 
          )
        );
      },
    );
  
  }).toList(),
  
),
  ),
    Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: sliderDetail.map((url) {
              int index = sliderDetail.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                    ? Color.fromRGBO(62, 64, 149, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4),
                ),
              );
            }).toList(),
          ),
     
     
      // Container(
     

      //   child: new Text(
      //   'Lorem Ipsum is simply the dummy text of printing and typesetting industry.',
      //   style: TextStyle(fontSize: 20,
      //   color: subColor,
      //                     fontFamily: 'Poppins',
      //                   fontWeight: FontWeight.w600,
      //                   ),
      //   textAlign: TextAlign.center
      // ),),
      
   
//        IconButton(
//   icon: Image.asset('assets/logo.png'),
//   iconSize: 50,
//   onPressed: () {},
// ),
      
    Container(
  margin: const EdgeInsets.only(top: 15.0),
     width: 300.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Login',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createLoginRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
  borderRadius: BorderRadius.circular(18.0),
  side: BorderSide(color: Colors.red)
),
   
   

),
   decoration:new BoxDecoration(
    color: buttonColor,
   
  
    shape: BoxShape.circle,
  ),
    
       ),
  //      InkWell(
  //           onTap: () {},
  //            child: Container(
  // margin: const EdgeInsets.only(top: 15.0),
  //    width: 300.0,
  // height: 50,
  // child: Image.asset('assets/btn.png',
  //       width: 110.0, height: 110.0),
  //             )
  //      ),
    
       Container(
  margin: const EdgeInsets.only(top: 15.0),
     width: 300.0,
  height: 50,
  
  child: FlatButton(
  child: SizedBox(
    width: double.infinity,
    child: Text(
      'Register',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
      Navigator.of(context).push(_createRegisterRoute());
  //    Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Register()),
  // );
    
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
),
 
    
       ),
    ],
  ),
),
      
    );
  }
  }
  

//My LOGIN PAGE
class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();

    
  }
}
class LoginState extends State <Login>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
 bool _obscureText = true;

  _toggle() {
    setState(() {
      _obscureText = !_obscureText;
      return _obscureText;
    });
  }

 Route _createRegisterRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Register(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

Route _createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}





Route _createForgotRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => ForgotPassword(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      new Container(
        padding: new EdgeInsets.only(top:30),
       child: Image.asset('assets/logo.png',
       width:200,
       height:200
       ),
      ),

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Welcome Back",
        style: TextStyle(fontSize: 30,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),
        new Container(
        padding: new EdgeInsets.only(top:10),
        child: Text("Login to your account",
        style: TextStyle(fontSize: 15,
                        color:Colors.black.withOpacity(0.6),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),

      )
      ),
      new Container(
        margin: new EdgeInsets.only(top:10),
        padding: new EdgeInsets.all(20),
        child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            obscureText: _obscureText,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
     suffixIcon:  Icon(Icons.visibility_outlined,
                color: Colors.grey,
                ),
            
              hintText: 'Password',
             
            ),
            
            validator: (value) {
              if (value.isEmpty) {
                return 'Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }
              return null;
            },
          ),

 
Padding(child:     new GestureDetector(
  onTap: () {
  
    Navigator.of(context).push(_createForgotRoute());
    
  },
  
  child: new Text('Forgot your Password?',

          style: TextStyle(fontSize: 18,
          color:inputColor.withOpacity(0.8),
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w800)
)
),
padding: const EdgeInsets.symmetric(vertical: 16.0),
),
     
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(
               width: 70.0,
                height: 70.0,
                margin: EdgeInsets.only(left: 280),
           
                 child: FloatingActionButton
            (
              
               backgroundColor: buttonColor,
                foregroundColor: Colors.black,
                
 
  
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_formKey.currentState.validate()) {
                  // Process data.
                   Navigator.of(context).push(_createHomeRoute());
                }
               
              },
               child: Icon(Icons.east,color: Colors.white,size: 50),
             
            ),
            ),

           
           
          ),
        ],
      ),
    )
      ),

      new Container(
       margin: EdgeInsets.only(top:60),
          child: Row(
           mainAxisAlignment: MainAxisAlignment.center,
            children: [

          Text('Dont have an account?',
          style: TextStyle(fontSize: 18,
          color:Colors.black.withOpacity(0.6),
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w400)
),
new GestureDetector(
  onTap: () {

       Navigator.of(context).push(_createRegisterRoute());
    
  },
  child: new Text('  Sign Up',

          style: TextStyle(fontSize: 18,
          color:buttonColor,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w800)
)
),

       ],) 
        
      )



     
    ],
  ),
),
    );
  }
}



//My Registration Page
class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterState();

    
  }
}

class RegisterState extends State <Register>{
  var myName = TextEditingController();
  var myAddress = TextEditingController();
  var myLandmark = TextEditingController();
  var myCity = TextEditingController();
  var myDistrict = TextEditingController();
  var myState = TextEditingController();
   var myPincode = TextEditingController();
   var myMobile = TextEditingController();
   var myEmail = TextEditingController();
   var myPassword = TextEditingController();
  final myConfirmedPass = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Color buttonColor = Color(0xff3e4095);
  Color inputColor = Color(0xff626161);
  var appDevID = "";
 
  ScrollController _controller;


  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


  _makePostRequest(userObj) async {
    print("here body");
    // set up POST request arguments
    var body = jsonEncode(userObj);
    print(body);

    String url = 'http://173.212.223.65:80/v1/app/register';
    Map<String, String> headers = {"Content-Type": "application/json"};

    // make POST request
    Response response = await post(url, headers: headers, body:body);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    String res = response.body;

    print(response.body);
    if(response.statusCode==200){
      var myObj = jsonDecode(response.body);
    userObj.appID = myObj.appID;
    userObj.otp = myObj.otp;
      Navigator.of(context).push(_createVerifyRoute(userObj));
    }else{

    }

  }

  Route _createVerifyRoute(userObj) {
    var myUserObj = json.encode(userObj);
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => VerifyOtp(myUserObj),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

Route _createLoginRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => Login(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}
  @override
 
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
        _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        setState(() {
          
        });
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        setState(() {
        
        });
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        setState(() {
          
        });
        print("onResume: $message");
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        print("Push Messaging token: $token");
       appDevID= token;
      });
          print("appDevID: $appDevID");
    });
  }
   @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myName.dispose();
     myAddress.dispose();
      myCity.dispose();
       myDistrict.dispose();
        myDistrict.dispose();
         myState.dispose();
          myPincode.dispose();
           myMobile.dispose();
            myEmail.dispose();
             myPassword.dispose();
              myConfirmedPass.dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the bottom";
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the top";
      });
    }
  }

   String validateEmail(String value) {
      if (value.isEmpty) {
        return 'Email is required.';
      }

      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email';
      else
        return null;
    }

  Widget build(BuildContext context) {
   
    return Scaffold(
      
      resizeToAvoidBottomPadding: true,
     body:    
     
     
     Container(
       
        decoration: BoxDecoration(
          image: DecorationImage(
            
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
            width: double.maxFinite,
        height: double.maxFinite, 
      
  child: new ListView(
     controller: _controller,
  
    children: <Widget>[

       new Container(
         alignment: Alignment.center,
        padding: new EdgeInsets.only(top:20),
        child: Text("Welcome",
        style: TextStyle(fontSize: 25,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,
                        ),

      )
      ),
        new Container(
           alignment: Alignment.center,
        padding: new EdgeInsets.only(top:2),
        child: Text("Create an account",
        style: TextStyle(fontSize: 15,
                        color:Colors.black.withOpacity(0.6),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),

      )
      ),
    
    new Container(
      padding: EdgeInsets.only(left: 20,right: 20),
      child:Form(  
        
      key: _formKey,  
      child: Column(  
        
        crossAxisAlignment: CrossAxisAlignment.start,  
        children: <Widget>[  
         TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Name',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myName,

             validator: (value) {
              if (value.isEmpty) {
                return 'Name is required.';
              }else{
                print(value);
              }
              return null;
            },
             onSaved: (value) {
        setState(() {
       
        });
      },
           ),   
        TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Address',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myAddress,

             validator: (value) {
              if (value.isEmpty) {
                return 'Address is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),   
         TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Landmark',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myLandmark,

           ),  
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'City',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myCity,

             validator: (value) {
              if (value.isEmpty) {
                return 'City is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'District',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myDistrict,

             validator: (value) {
              if (value.isEmpty) {
                return 'District is required.';
              }else{
                print(value);
              }
              return null;
            },
           ),  
           new Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: <Widget>[
    new Flexible(
      child: new TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'State',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myState,

             validator: (value) {
              if (value.isEmpty) {
                return 'State is required.';
              }else{
                print(value);
              }
              return null;
            },
           ), 
    ),
    SizedBox(width: 20.0,),
    new Flexible(
      child: new TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Pincode',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            keyboardType: TextInputType.number,
            controller: myPincode,

             validator: (value) {
              if (value.isEmpty) {
                return 'Pincode is required.';
              }else if(value.length!=6){
return 'Enter valid 6 digit pincode.';
              }
              else{
                print(value);
              }
              return null;
            },
           ), 
    ),
   
    
  ],
),
TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Mobile number',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            keyboardType: TextInputType.number,
            controller: myMobile,

             validator: (value) {
              if (value.isEmpty) {
                return 'Mobile number is required.';
              }else if(value.length<10){
                return 'Please enter 10 digit mobile number.';
              }else{
                print(value);
              }
              return null;
            },
           ), 
           TextFormField(  
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ), 
              decoration: const InputDecoration(
                 hintText: 'Email address',  
          
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
            
            
             
            ),
            controller: myEmail,

            

             validator: validateEmail
           ), 
            TextFormField(
            obscureText: true,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
              suffixIcon: const Icon(Icons.visibility_outlined,
              color: Colors.grey),
              hintText: 'Password',
             
            ),
            controller: myPassword,

            validator: (value) {
              if (value.isEmpty) {
                return 'Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }else{
                print(value);
              }
              return null;
            },
            onSaved: (value) {
        setState(() {
      
        });
      },
          ),
           TextFormField(
            obscureText: true,
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
              suffixIcon: const Icon(Icons.visibility_outlined,
              color: Colors.grey),
              hintText: 'Confirm password',
             
            ),
            controller: myConfirmedPass,

            validator: (value) {
              if (value.isEmpty) {
                return 'Confirm Password is required.';
              }else if(value.length<6){
                return 'Please enter atleast 6 characters';
              }
              else if(value!=myPassword.text){
                return 'Password does not match!';
              }
              return null;
            },
          ),
           Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(
               width: 70.0,
                height: 70.0,
                margin: EdgeInsets.only(left: 280),
           
                 child: FloatingActionButton
            (
              
               backgroundColor: buttonColor,
                foregroundColor: Colors.black,
                
 
  
            onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_formKey.currentState.validate()) {

                  // Process data.
                    // Process data.

                  var location = {
                    'address':myAddress.text,
                    'pin':myPincode.text,
                    'city':myCity.text,
                    'dist':myDistrict.text,
                    'state':myState.text,
                    'landmark':myLandmark.text,
                  };
                  var userObj ={


                    'name':myName.text,
                    'mobileNo':myMobile.text,
                    'emailID':myEmail.text,
                    'passwd':myPassword.text,
                    'cmPasswd':myConfirmedPass.text,
                    'appDevID':appDevID,
                    'fcmKey':appDevID,
                    'location':location
                  };
                  print(userObj);

                  _makePostRequest(userObj);



              //
              //   var url ='http://173.212.223.65:80/v1/app/register';
              //   var body = jsonEncode({ 'data': userObj });
              //
              //   print("Body: " + body);
              //
              //
              //
              //   http.post(Uri.encodeFull(url),
              //       headers: {"Content-Type": "application/json"},
              //       body: body
              //   ).then((http.Response response) {
              //     print("Response status: ${response.statusCode}");
              // print("below");
              //     print(response);
              //     print(response.request);
              //     if(response.statusCode==200){
              //       Navigator.of(context).push(_createVerifyRoute());
              //     }else{
              //
              //     }
              //
              //   });






              

                }
               
              },
               child: Icon(Icons.east,color: Colors.white,size: 50),
             
            ),
            ),

           
           
          ),
           
        ],  
      ),  
    )
    ),

    new Container(
      padding: new EdgeInsets.all(20),
       margin: EdgeInsets.only(top:20),
          child: Row(
           mainAxisAlignment: MainAxisAlignment.center,
            children: [

          Text('Already have an account?',
          style: TextStyle(fontSize: 18,
          color:Colors.black.withOpacity(0.6),
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w400)
),
new GestureDetector(
  onTap: () {
   
     
      Navigator.of(context).push(_createLoginRoute());
    
  },
  child: new Text('  Login',

          style: TextStyle(fontSize: 18,
          color:buttonColor,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w800)
)
),

       ],) 
        
      )
      

  
   
     
   

    
      
    ],
  ),
),
      
   
    );
  }
}


class ForgotPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ForgotPasswordState();

    
  }
}
class ForgotPasswordState extends State <ForgotPassword>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
 

  String validateEmail(String value) {
      if (value.isEmpty) {
        return 'Email is required.';
      }

      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email';
      else
        return null;
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      new Container(
        padding: new EdgeInsets.only(top:30),
       child: Image.asset('assets/logo.png',
       width:200,
       height:200
       ),
      ),

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Forgot password?",
        style: TextStyle(fontSize: 32,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),
        new Container(
         
        padding: new EdgeInsets.only(top:10,left:60,right: 60),
        alignment: Alignment.center,
        child: Text("Please enter your registered email address.",
        style: TextStyle(fontSize: 14,
                        color:Colors.black.withOpacity(0.5),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),
textAlign: TextAlign.center,
      )
      ),
      new Container(
        margin: new EdgeInsets.only(top:10),
        padding: new EdgeInsets.all(20),
        child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
             
             
              hintText: 'demo@ps.com',
             
            ),
            validator: validateEmail
          ),

          Padding(
            
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(

               width: 70.0,
                height: 70.0,
                margin: EdgeInsets.only(left: 280,top: 25),
           
                 child: FloatingActionButton
            (
              
               backgroundColor: buttonColor,
                foregroundColor: Colors.black,
                
 
  
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_formKey.currentState.validate()) {
                  // Process data.
                }
               
              },
               child: Icon(Icons.east,color: Colors.white,size: 50),
             
            ),
            ),

           
           
          ),
        ],
      ),
    )
      ),

     



     
    ],
  ),
),
    );
  }
}

class VerifyOtp extends StatefulWidget {
  final String userObj;//if you have multiple values add here
  VerifyOtp(this.userObj, {Key key}): super(key: key);
  @override

  VerifyOtpState createState() => new VerifyOtpState();
}
class VerifyOtpState extends State <VerifyOtp>{

final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
var myOtp = TextEditingController();
 Route _createSuccessRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => RegisterSuccess(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

_makePostRequest(userObj) async {
  print("here body");
  // set up POST request arguments
  var body = jsonEncode(userObj);
  print(body);

  String url = 'http://173.212.223.65:80/v1/app/validate';
  Map<String, String> headers = {"Content-Type": "application/json"};

  // make POST request
  Response response = await post(url, headers: headers, body:body);
  // check the status code for the result
  int statusCode = response.statusCode;
  // this API passes back the id of the new item added to the body
  String res = response.body;
  print(response.body);
  if(response.statusCode==200){
    Navigator.of(context).push(_createSuccessRoute());
  }else{

  }

}

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
     
Align(
                alignment: Alignment.centerLeft,
     child: new IconButton(
            padding: new EdgeInsets.only(top:50,left: 10),
       alignment: Alignment.centerLeft,
      
        icon: new Icon(Icons.keyboard_backspace,size: 50.0,color: inputColor,
        
  ),
  onPressed: () {
  Navigator.pop(context);
}
      ),
),
      
     

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Enter OTP",
        style: TextStyle(fontSize: 32,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),
        new Container(
         
        padding: new EdgeInsets.only(top:10,left:40,right: 40),
        alignment: Alignment.center,
        child: Text("Please verify your mobile / email to continue. Otp sent on mobile number *01233646373 / email * test@demo.com",
        style: TextStyle(fontSize: 14,
                        color:Colors.black.withOpacity(0.5),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        ),
textAlign: TextAlign.center,
      )
      ),
      new Container(
        margin: new EdgeInsets.only(top:10),
        padding: new EdgeInsets.all(20),
        child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            keyboardType: TextInputType.number,
            controller: myOtp,

            validator: (value) {
              if (value.isEmpty) {
                return 'Otp is required.';
              }else if(value.length<4){
                return 'Please enter 4 digit otp sent to your email or mobile.';
              }else{
                var myUserObj = json.decode(widget.userObj);
                var reqObj = {
                  "otp":myOtp,
                  "appDevID":myUserObj.fcmKey,
                  "appID":myUserObj.appID,
                };
                _makePostRequest(reqObj);
              }
              return null;
            },
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
             
             
              hintText: '123456',
             
            ),
           
          ),

          Padding(
            
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(

               width: 70.0,
                height: 70.0,
                margin: EdgeInsets.only(left: 280,top: 25),
           
                 child: FloatingActionButton
            (
              
               backgroundColor: buttonColor,
                foregroundColor: Colors.black,
                
 
  
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                // if (_formKey.currentState.validate()) {
                //   // Process data.
                // }
  // Navigator.of(context).push(_createSuccessRoute());
  //               var myReqObj={
  //                 "otp":myOtp,
  //                 "appDevID":this.resp,
  //                 "appID":this.resp.appID,
  //
  //               };
  //               _makePostRequest(myReqObj);
               
              },
               child: Icon(Icons.east,color: Colors.white,size: 50),
             
            ),
            ),

           
           
          ),
        ],
      ),
    )
      ),

     



     
    ],
  ),
),
    );
  }
}



//Registration Success Screen


class RegisterSuccess extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterSuccessState();

    
  }
}
class RegisterSuccessState extends State <RegisterSuccess>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);
 bool _obscureText = true;

  

 Route _createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: new Column(
    
    
    children: <Widget>[
      // Image.asset('logo.png',width:300,height:100),
      Align(
                alignment: Alignment.centerLeft,
     child: new IconButton(
            padding: new EdgeInsets.only(top:50,left: 10),
       alignment: Alignment.centerLeft,
      
        icon: new Icon(Icons.keyboard_backspace,size: 50.0,color: inputColor,
        
  ),
  onPressed: () {
  Navigator.pop(context);
}
      ),
),

      new Container(
        padding: new EdgeInsets.only(top:50),
       child: Image.asset('assets/check.png',
       width:100,
       height:100
       ),
      ),

      new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Success!",
        style: TextStyle(fontSize: 30,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w900,),

      )
      ),
        new Container(
      
       padding: new EdgeInsets.all(10),
        child: Text("Your account has been successfully created!",
         textAlign: TextAlign.center,
        style: TextStyle(fontSize: 21,
                        color:Colors.black.withOpacity(0.6),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,
                        
                        ),

      )
      ),
     Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(
               width: 70.0,
                height: 70.0,
                margin: EdgeInsets.only(left: 280),
           
                 child: FloatingActionButton
            (
              
               backgroundColor: buttonColor,
                foregroundColor: Colors.black,
                
 
  
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                  Navigator.of(context).push(_createHomeRoute());
               
              },
               child: Icon(Icons.east,color: Colors.white,size: 50),
             
            ),
            ),

           
           
          ),

     



     
    ],
  ),
),
    );
  }
}

