import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';


class MyOrder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return MyOrderState();

    
  }
}
class MyOrderState extends State <MyOrder>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
//  Route _createSuccessRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => RegisterSuccess(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
   extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "My Order",
                style: TextStyle(color: Colors.black,
                fontSize: 26,
                fontFamily: "Poppins",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: Container(
    margin: EdgeInsets.only(top:100),
    child:(Column(
mainAxisSize: MainAxisSize.min,
crossAxisAlignment: CrossAxisAlignment.start,
    

      children :[
        Card(
        elevation: 0,
      color: Colors.white.withOpacity(0.7),
          child: Column(children: [
Row(
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: [
 new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Order : 04 Sep 20",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,),

      )
      ),
       new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Delivered : 08 Sep 20",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,),

      )
      ),
],),
Column(
  mainAxisSize: MainAxisSize.max,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
Padding(
  padding: EdgeInsets.all(10),
  child:Card(
  margin: EdgeInsets.all(10),
  child: Column(children: [
  Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
    Padding(
      padding: EdgeInsets.all(5),
      child:new Container(
        padding: new EdgeInsets.only(top:10),
        child: Text("Type 1 weighing scales",
        style: TextStyle(fontSize: 18,
                        color:buttonColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,),

      )
      ),)
  ],),
  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.all(10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:10,top:10),
        child: Text("Order Total",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
      Padding(
           padding:  EdgeInsets.all(10),
        child:new Container(
        padding: new EdgeInsets.only(bottom:10,top:10),
        child: Text("₹ 5550",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),)
  ],)
],),))
],)
          ],
          )
          ),
           Card(
        elevation: 0,
      color: Colors.white.withOpacity(0.7),
          child: Column(children: [
Row(
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: [
 new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Order : 04 Sep 20",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,),

      )
      ),
       new Container(
        padding: new EdgeInsets.only(top:30),
        child: Text("Delivered : 08 Sep 20",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,),

      )
      ),
],),
Column(
  mainAxisSize: MainAxisSize.max,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
Padding(
  padding: EdgeInsets.all(10),
  child:Card(
  margin: EdgeInsets.all(10),
  child: Column(children: [
  Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
    Padding(
      padding: EdgeInsets.all(5),
      child:new Container(
        padding: new EdgeInsets.only(top:10),
        child: Text("Type 2 weighing scales",
        style: TextStyle(fontSize: 18,
                        color:buttonColor,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,),

      )
      ),)
  ],),
  Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
    Padding(
      padding:  EdgeInsets.all(10),
      child:new Container(
        padding: new EdgeInsets.only(bottom:10,top:10),
        child: Text("Order Total",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),),
      Padding(
           padding:  EdgeInsets.all(10),
        child:new Container(
        padding: new EdgeInsets.only(bottom:10,top:10),
        child: Text("₹ 5580",
        style: TextStyle(fontSize: 16,
                        color:Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,),

      )
      ),)
  ],)
],),))
],)
          ],
          )
          ),
      ]
    )
    )
      
    )
),
    );
  }
}