
import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';



class AddGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return AddGroupState();

    
  }
}
class AddGroupState extends State <AddGroup>{
 

 
  
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
 Route _createSuccessRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AddGroupDetails(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
  
      extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
  actions: [
    GestureDetector(child:
    Icon(Icons.done,size: 40.0,color: inputColor,),
      
  
   onTap: (){
                      Navigator.of(context).push(_createSuccessRoute());
      },
   )
  ],
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Create Group",
                style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
         decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)),
        child:Stack(
          children: <Widget>[
            
            Container(
              
            child: SingleChildScrollView(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                
                children: <Widget>[
                 
                  Container(
                    margin: EdgeInsets.only(top:100),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children:[
                          CircleAvatar(
              radius: 25,
          
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/user.png'),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:5.0),
            child: Text('Customer Name',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),),)
                          
                        ]
                        
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top:100),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children:[
                          CircleAvatar(
              radius: 25,
          
              child: CircleAvatar(
            
                radius: 50,
                backgroundImage: AssetImage('assets/user.png'),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:5.0),
            child: Text('Customer Name',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),),)
                          
                        ]
                        
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top:100),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children:[
                          CircleAvatar(
              radius: 25,
          
              child: CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/user.png'),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:5.0),
            child: Text('Customer Name',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),),)
                          
                        ]
                        
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                 
                  
                ],
              ),
            ),
          ),
            DraggableScrollableSheet(
               initialChildSize: 0.75,
            minChildSize: 0.1,
            maxChildSize: 0.77,
              builder: (BuildContext context, ScrollController scrollController){
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(30), topLeft: Radius.circular(30.0)),
                    color: Colors.white,

                  ),
                  child: ListView.builder(
                      controller: scrollController,
                      itemCount: 25,
                      itemBuilder: (BuildContext context, int index){
                    return ListTile(
                      leading: Icon(Icons.person_outline,
                      color: buttonColor,),
                      title : Text('Customer $index'),
                      );
                  }),
                );
              },
            )
        ],),)
    );
  }
}


class AddGroupDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return AddGroupDetailsState();

    
  }
}
class AddGroupDetailsState extends State <AddGroupDetails>{
 
final _formKey = GlobalKey<FormState>();
  int _currentIndex = 1;
  
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
 Route _createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => HomePage(),
   transitionsBuilder: (context, animation, secondaryAnimation, child) {
    return SlideTransition(
                    transformHitTests: false,
                    position: new Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: Offset.zero,
                    ).animate(animation),
                    child: new SlideTransition(
                      position: new Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(-1.0, 0.0),
                      ).animate(secondaryAnimation),
                      child: child,
                    ),
                  );
},
  );
}

 _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Weighing scale',
            style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),),
            content: RadioListTile(
              title: Text("Weighing scale name",
              style: TextStyle(color: Colors.grey,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),),
              groupValue: _currentIndex,
              value: 1,
              onChanged: (val) {
                setState(() {
                  _currentIndex = val;
                });
              },
            ),
            actions: <Widget>[
              
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children:[
                 new FlatButton(
                child: new Text('CANCEL',style: TextStyle(color: Colors.grey,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Container(
  margin: const EdgeInsets.all(20.0),
     width: 130.0,
  height: 50,
  child: FlatButton(
   
  child: SizedBox(
    width: double.infinity,
    child: Text(
    
      'Buy now',
      textAlign: TextAlign.left,
    ),
  ),
  
  onPressed: () {
    
     Navigator.of(context).push(_createHomeRoute());
  //   Navigator.push(
  //   context,
  //   MaterialPageRoute(builder: (context) => Login()),
  // );
  },
   color: buttonColor,
      textColor: Colors.white,
     shape: RoundedRectangleBorder(
        
  
        borderRadius: BorderRadius.circular(25.0),
         
        side: BorderSide(color: buttonColor)),
   
   

),
 
    
       ),
               ]
             )
            ],
          );
        });
  }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
  
      extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Create Group",
                style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
         decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)),
        child:Stack(
          children: <Widget>[
            
           new Container(
        margin: new EdgeInsets.only(top:70),
        padding: new EdgeInsets.all(20),
        child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
           
            textAlign: TextAlign.left,
            cursorColor: inputColor,
            
            style:TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
              ),
            decoration: const InputDecoration(
              enabledBorder: UnderlineInputBorder(      
	            borderSide: BorderSide(color: Colors.grey),   
              
	  ),  
      focusedBorder: UnderlineInputBorder(
	  borderSide: BorderSide(color: Colors.grey),
	   ),
   
            
              hintText: 'Type group subject here..',
             
            ),
            
            validator: (value) {
              if (value.isEmpty) {
                return 'Group subject is required.';
              }
              return null;
            },
          ),
    //     GestureDetector(child:TextFormField(
           
    //         textAlign: TextAlign.left,
    //         cursorColor: inputColor,
            
    //         style:TextStyle(
    //         color:inputColor,
    //         fontSize:20,
    //         fontFamily: 'Poppins',
    //         fontWeight: FontWeight.w900
    //           ),
    //         decoration: const InputDecoration(
    //           enabledBorder: UnderlineInputBorder(      
	  //           borderSide: BorderSide(color: Colors.grey),   
              
	  // ),  
    //   focusedBorder: UnderlineInputBorder(
	  // borderSide: BorderSide(color: Colors.grey),
	  //  ),
    //  suffixIcon:  Icon(Icons.arrow_drop_down_outlined,
    //             color: Colors.grey,
              
    //             ),
            
    //           hintText: 'Select weighing scale',
             
    //         ),
            
    //        onTap: (){
    //          _radioAlertRoute();
           
    //        },
    //       )),


    Container(
      margin: EdgeInsets.only(top:20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
Text("Select weighing scale.",
style: TextStyle(
            color:inputColor,
            fontSize:20,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w900
),),
GestureDetector(
  onTap: () => _displayDialog(context),
  
  child:Icon(Icons.arrow_drop_down_outlined))

             
    ],),
   decoration: BoxDecoration(
     
          border: Border(
           
            bottom: BorderSide(width: 1.0, color: Colors.grey),
          ),
       
        ),)


  



     
          
        ],
      ),
    )
      ),
            DraggableScrollableSheet(
               initialChildSize: 0.7,
            minChildSize: 0.1,
            maxChildSize: 0.75,
              builder: (BuildContext context, ScrollController scrollController){
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(30), topLeft: Radius.circular(30.0)),
                    color: Colors.white,

                  ),
                  child: ListView.builder(
                      controller: scrollController,
                      itemCount: 2,
                      itemBuilder: (BuildContext context, int index){
                    return ListTile(
                      leading: Icon(Icons.person_outline,
                      color: buttonColor,),
                      title : Text('Customer $index'),
                      );
                  }),
                );
              },
            )
        ],),)
    );
  }
}


//GroupLIST 












