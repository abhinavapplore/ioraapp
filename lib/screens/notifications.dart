import 'package:flutter/material.dart';

import 'package:iora/screens/home_screen.dart';


class Notifications extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState



    return NotificationsState();

    
  }
}
class NotificationsState extends State <Notifications>{
final _formKey = GlobalKey<FormState>();
 Color buttonColor = Color(0xff3e4095);
 Color inputColor = Color(0xff626161);

 
 
//  Route _createSuccessRoute() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => RegisterSuccess(),
//    transitionsBuilder: (context, animation, secondaryAnimation, child) {
//     return SlideTransition(
//                     transformHitTests: false,
//                     position: new Tween<Offset>(
//                       begin: const Offset(1.0, 0.0),
//                       end: Offset.zero,
//                     ).animate(animation),
//                     child: new SlideTransition(
//                       position: new Tween<Offset>(
//                         begin: Offset.zero,
//                         end: const Offset(-1.0, 0.0),
//                       ).animate(secondaryAnimation),
//                       child: child,
//                     ),
//                   );
// },
//   );
// }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
   extendBodyBehindAppBar: true,
      appBar: new AppBar(
        centerTitle: true,
        leading: IconButton(
    icon: Icon(Icons.keyboard_backspace,size: 40.0,color: inputColor,),
    onPressed: () => Navigator.of(context).pop(),
  ), 
         backgroundColor: Colors.transparent,
              elevation: 0.0,
              title: new Text(
                "Notification",
                style: TextStyle(color: Colors.black87,
                fontSize: 26,
                fontFamily: "Poppins-Bold",
                ),
              
              ),
             
            ),
      resizeToAvoidBottomPadding: false,
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
	image: AssetImage("assets/main-bg2.png"),
  fit: BoxFit.cover)
        ),
         
      
  child: Container(
    margin: EdgeInsets.only(top:100),
    child:(Column(
mainAxisSize: MainAxisSize.min,
crossAxisAlignment: CrossAxisAlignment.start,
    

      children :[
        Card(
          shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
               child:ClipPath(
                  clipper: ShapeBorderClipper(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15))),
                  child: Container(
               
                      decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(color: buttonColor, width: 5)),
                        color: Colors.white,
                      ),
                   
                      alignment: Alignment.centerLeft,
                      child: Column(
          
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
          
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
              padding: EdgeInsets.only(left: 12,right: 12),
              child:Text("Request",
              style: TextStyle(color: buttonColor,fontSize: 18,fontFamily: "Poppins-Bold"),),),
              Container(
                padding: EdgeInsets.only(left: 12,right: 12),
              child: Text("Sender Name",
                style: TextStyle(color: Colors.black87.withOpacity(0.9),fontSize: 14,fontFamily: "Poppins-Bold"),),)
                
            ],
            ),
           
                Container(
                  padding: EdgeInsets.only(left: 12,right: 12),
               
                  child: Text("Lorem ipsum is simply a dummy text for printing and typesetting industry",
                  style: TextStyle(fontSize: 14,color: inputColor.withOpacity(0.8),fontFamily: "Poppins-Regular"),),
                )
              ,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                 
            height: 20.0,
                  margin: EdgeInsets.only(right:95),
                  child: Text("10 mins ago",
                  style: TextStyle(fontSize: 14,color: inputColor.withOpacity(0.8),fontFamily: "Poppins-Regular"),),),
                 FlatButton(
                      height: 30,
          onPressed: null,
          child: Text('Accept', style: TextStyle(
              color: buttonColor
            )
          ),
        
          shape: RoundedRectangleBorder(side: BorderSide(
            color: buttonColor,
            width: 1,
            style: BorderStyle.solid
          ), borderRadius: BorderRadius.circular(50)),
        )
               ,
              Container( 
                margin: EdgeInsets.only(left:10,right: 10),
                child:FlatButton(
              height: 30,
          onPressed: null,
          child: Text('Reject', style: TextStyle(
              color: Colors.red
            )
          ),
        
          shape: RoundedRectangleBorder(side: BorderSide(
            color: Colors.red,
            width: 1,
            style: BorderStyle.solid
          ), borderRadius: BorderRadius.circular(50)),
        ))
              ],
            ),
          ],
        ),),
                ),
        

        
      ),
      ]
    )
    )
      
    )
),
    );
  }
}