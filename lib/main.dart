import 'package:flutter/material.dart';
import 'package:iora/screens/home_screen.dart';
// import 'package:my_first_app/screens/home_screen.dart';
import 'package:iora/screens/introduction.dart';
import 'package:iora/screens/notifications.dart';
import 'package:iora/screens/addGroup.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       routes: <String, WidgetBuilder>{
        "/introduction": (BuildContext context) => new FirstRoute(),
        "/home": (BuildContext context) => new HomePage(),
        "/notifications": (BuildContext context) => new Notifications(),
         "/addGroup": (BuildContext context) => new AddGroup(),
      },
      initialRoute: "/home",
      title: 'Iora',
       debugShowCheckedModeBanner: false,
       
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: FirstRoute());
  }
}